Simulator
Simulates game actions and optionally solves the whole game

Dependencies
Install with your favorite package manager

Build
Compile with tsc or run with ts-node

Command line options

index.js <command string>
Execute commands from string and report the results.
Command is a string of numbers 1 through 8.
Make it a permutations if you want to follow the rules.

index.js solve
Solve the game, place the solutions in solutions.txt