/*
    Copyright 2024 Gironta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

import * as fs from "fs";
import * as path from "path";
import Simulator from "./simulator";

describe("Simulator", () => {
  let simulator: Simulator;

  beforeEach(() => {
    simulator = new Simulator();
  });

  it("exists", () => {
    expect(Simulator).toBeDefined();
  });

  it("holds the simulation variables", () => {
    expect(simulator).toBeDefined();
    expect(simulator.fullness).toEqual(0);
    expect(simulator.futa).toEqual(0);
    expect(simulator.corruption).toEqual(0);
    expect(simulator.boobs).toEqual(0);
    expect(simulator.weakness).toEqual(0);
  });

  it("has 8 numbered simulation methods", () => {
    expect(simulator.one).toBeDefined();
    expect(simulator.two).toBeDefined();
    expect(simulator.three).toBeDefined();
    expect(simulator.four).toBeDefined();
    expect(simulator.five).toBeDefined();
    expect(simulator.six).toBeDefined();
    expect(simulator.seven).toBeDefined();
    expect(simulator.eight).toBeDefined();
  });

  it("can be reset", () => {
    simulator.fullness = 1;
    simulator.corruption = 2;
    simulator.reset();
    expect(simulator.fullness).toEqual(0);
    expect(simulator.futa).toEqual(0);
    expect(simulator.corruption).toEqual(0);
    expect(simulator.boobs).toEqual(0);
    expect(simulator.weakness).toEqual(0);
  });

  it("reports its state", () => {
    expect(simulator.state).toEqual("0000");
    simulator.fullness = 1;
    simulator.boobs = 2;
    expect(simulator.state).toEqual("1002");
  });

  describe("simulation variables", () => {
    /**
     * Commented because not true. Variables can go
     * above the value reported as max and they will
     * still show as max in the ui. They also act
     * as their value above max when decreasing.
     */
    // it("cannot increase above 2", () => {
    //   simulator.fullness++;
    //   simulator.fullness++;
    //   simulator.fullness++;
    //   simulator.fullness++;
    //   expect(simulator.fullness).toEqual(2);
    // });

    // it("cannot be assigned above 2", () => {
    //   simulator.boobs = 4;
    //   expect(simulator.boobs).toEqual(2);
    // });

    // it("cannot increase above 2 in one operation", () => {
    //   simulator.corruption = 1;
    //   simulator.corruption += 2;
    //   expect(simulator.corruption).toEqual(2);
    // });

    it("cannot decrease below 0", () => {
      simulator.fullness--;
      expect(simulator.fullness).toEqual(0);
    });

    it("weakness cannot increase above 1", () => {
      simulator.weakness = 4;
      expect(simulator.weakness).toEqual(1);
    });
  });

  describe("one", () => {
    it("increases boobs if weak", () => {
      simulator.weakness = 1;
      simulator.one();
      expect(simulator.boobs).toEqual(1);
    });

    it("increases boobs if full", () => {
      simulator.fullness = 1;
      simulator.one();
      expect(simulator.boobs).toEqual(1);
    });

    it("increases boobs if boobs", () => {
      simulator.boobs = 1;
      simulator.one();
      expect(simulator.boobs).toEqual(2);
    });

    it("does not increase boobs if not weak, full or boobs", () => {
      simulator.one();
      expect(simulator.boobs).toEqual(0);
    });

    it("decreases futa if weak", () => {
      simulator.weakness = 1;
      simulator.futa = 1;
      simulator.one();
      expect(simulator.futa).toEqual(0);
    });

    it("decreases futa if not weak", () => {
      simulator.futa = 1;
      simulator.one();
      expect(simulator.futa).toEqual(0);
    });
  });

  describe("two", () => {
    it("increases futa if weak", () => {
      simulator.weakness = 1;
      simulator.two();
      expect(simulator.futa).toEqual(1);
    });

    it("increases futa if boobs big", () => {
      simulator.boobs = 2;
      simulator.two();
      expect(simulator.futa).toEqual(1);
    });

    it("increases futa if max corrupt", () => {
      simulator.corruption = 2;
      simulator.two();
      expect(simulator.futa).toEqual(1);
    });

    it("decreases boobs if not weak and not boobs big", () => {
      simulator.boobs = 1;
      simulator.two();
      expect(simulator.boobs).toEqual(0);
    });
  });

  describe("three", () => {
    it("increases weakness", () => {
      simulator.three();
      expect(simulator.weakness).toEqual(1);
    });

    it("increases corruption if corrupt", () => {
      simulator.corruption = 1;
      simulator.three();
      expect(simulator.corruption).toEqual(2);
    });

    it("increases corruption if futa", () => {
      simulator.futa = 1;
      simulator.three();
      expect(simulator.corruption).toEqual(1);
    });
  });

  describe("four", () => {
    it("increases fullness if weak", () => {
      simulator.weakness = 1;
      simulator.four();
      expect(simulator.fullness).toEqual(1);
    });

    it("does nothing if not weak", () => {
      simulator.four();
      expect(simulator.fullness).toEqual(0);
    });
  });

  describe("five", () => {
    it("if weak increases futa and corruption, decreases fullness", () => {
      simulator.weakness = 1;
      simulator.fullness = 1;
      simulator.five();
      expect(simulator.fullness).toEqual(0);
      expect(simulator.futa).toEqual(1);
      expect(simulator.corruption).toEqual(1);
    });

    it("if futa increases corruption, decreases fullness", () => {
      simulator.futa = 1;
      simulator.fullness = 1;
      simulator.five();
      expect(simulator.fullness).toEqual(0);
      expect(simulator.futa).toEqual(1);
      expect(simulator.corruption).toEqual(1);
    });
  });

  describe("six", () => {
    it("increases weakness", () => {
      simulator.six();
      expect(simulator.weakness).toEqual(1);
    });
    it("decreases futa", () => {
      simulator.futa = 1;
      simulator.six();
      expect(simulator.futa).toEqual(0);
    });
    it("decreases boobs", () => {
      simulator.boobs = 1;
      simulator.six();
      expect(simulator.boobs).toEqual(0);
    });
  });

  describe("seven", () => {
    it("increases boobs if normal", () => {
      simulator.seven();
      expect(simulator.boobs).toEqual(1);
    });

    it("increases fullness if weak", () => {
      simulator.weakness = 1;
      simulator.seven();
      expect(simulator.fullness).toEqual(1);
    });

    it("increases futa when boobs and not futa", () => {
      simulator.boobs = 1;
      simulator.seven();
      expect(simulator.futa).toEqual(1);
    });

    it("does nothing if boobs and futa and not weak", () => {
      simulator.boobs = 1;
      simulator.futa = 1;
      simulator.seven();
      expect(simulator.weakness).toEqual(0);
      expect(simulator.boobs).toEqual(1);
      expect(simulator.futa).toEqual(1);
    });
  });

  describe("eight", () => {
    function testStats() {
      expect(simulator.fullness).toEqual(2);
      expect(simulator.boobs).toEqual(1);
    }

    it("increases stats when weak", () => {
      simulator.weakness = 1;
      simulator.eight();
      testStats();
    });
    it("increases stats when corrupt", () => {
      simulator.corruption = 1;
      simulator.eight();
      testStats();
    });
    it("increases stats when full", () => {
      simulator.fullness = 1;
      simulator.eight();
      expect(simulator.fullness).toEqual(3);
      expect(simulator.boobs).toEqual(1);
    });
  });

  describe("execute", () => {
    it("test all known results", () => {
      const known_results = fs
        .readFileSync(path.join(__dirname, "known_results.txt"), "utf8")
        .split("\n");

      //sanity check
      expect(known_results[0]).toEqual("54281763 0000");

      for (const row of known_results) {
        const [input, output] = row.split(" ");
        simulator.execute(input);
        expect(simulator.state).toEqual(output);
        simulator.reset();
      }
    });
  });
});
