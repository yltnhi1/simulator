/*
    Copyright 2024 Gironta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

import { baseNStrings, clamp } from "./util";

describe("clamp", () => {
  it("clamps the value to the correct range", () => {
    expect(clamp(4, 0, 2)).toEqual(2);
    expect(clamp(-4, 0, 2)).toEqual(0);
    expect(clamp(1, 0, 2)).toEqual(1);
    expect(clamp(0, 0, 2)).toEqual(0);
    expect(clamp(2, 0, 2)).toEqual(2);
  });
});

describe("baseNStrings", () => {
  it("works for base 2", () => {
    const expected = [
      "0000",
      "0001",
      "0010",
      "0011",
      "0100",
      "0101",
      "0110",
      "0111",
      "1000",
      "1001",
      "1010",
      "1011",
      "1100",
      "1101",
      "1110",
      "1111",
    ];
    expect(baseNStrings(2, 4)).toEqual(expected);
  });
  it("works for base 16", () => {
    const expected = [
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
    ];
    expect(baseNStrings(16, 1)).toEqual(expected);
  });
  it("works for length 0", () => {
    const expected: string[] = [];
    expect(baseNStrings(8, 0)).toEqual(expected);
  });
  it("works for base 0, whatever that means", () => {
    const expected: string[] = [];
    expect(baseNStrings(0, 10)).toEqual(expected);
  });
  it("works for base 1", () => {
    expect(baseNStrings(1, 2)).toEqual(["00"]);
    expect(baseNStrings(1, 5)).toEqual(["00000"]);
  });
});
