/*
    Copyright 2024 Gironta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

import { clamp } from "./util";

export default class Simulator {
  private _fullness = 0;
  private _futa = 0;
  private _corruption = 0;
  private _boobs = 0;
  private _weakness = 0;

  public get fullness() {
    return this._fullness;
  }

  public set fullness(value: number) {
    value = this.adjustPropertyValue(value);
    this._fullness = value;
  }

  public get futa() {
    return this._futa;
  }

  public set futa(value: number) {
    value = this.adjustPropertyValue(value);
    this._futa = value;
  }

  public get corruption() {
    return this._corruption;
  }

  public set corruption(value: number) {
    value = this.adjustPropertyValue(value);
    this._corruption = value;
  }

  public get boobs() {
    return this._boobs;
  }

  public set boobs(value: number) {
    value = this.adjustPropertyValue(value);
    this._boobs = value;
  }

  public get weakness() {
    return this._weakness;
  }

  public set weakness(value: number) {
    value = clamp(value, 0, 1);
    this._weakness = value;
  }

  private adjustPropertyValue(value: number) {
    return clamp(value, 0, 5);
  }

  public get state() {
    return `${clamp(this.fullness, 0, 2)}${clamp(this.futa, 0, 2)}${clamp(
      this.corruption,
      0,
      2
    )}${clamp(this.boobs, 0, 2)}`;
  }

  public reset() {
    this.fullness = 0;
    this.futa = 0;
    this.corruption = 0;
    this.boobs = 0;
    this.weakness = 0;
  }

  /**sucker*/
  public one() {
    if (this.weakness > 0 || this.fullness > 0 || this.boobs > 0) {
      this.boobs++;
    }
    this.futa--;
    this.resetWeakness();
  }
  /**potion*/
  public two() {
    if (this.weakness > 0 || this.boobs == 2 || this.corruption == 2) {
      this.futa++;
    } else {
      this.boobs--;
    }
    this.resetWeakness();
  }
  /**gas*/
  public three() {
    if (this.corruption > 0 || this.futa > 0) {
      this.corruption++;
    }
    this.resetWeakness();
    this.weakness++;
  }
  /**slime*/
  public four() {
    if (this.weakness > 0) {
      this.fullness++;
    }
    this.resetWeakness();
  }
  /**imp*/
  public five() {
    if (this.weakness > 0 && !this.futa) {
      this.futa++;
    }
    if (this.futa > 0) {
      this.corruption++;
      this.fullness--;
    }
    this.resetWeakness();
  }
  /**weakening*/
  public six() {
    this.futa--;
    this.boobs--;
    this.resetWeakness();
    this.weakness++;
  }
  /**tentacle*/
  public seven() {
    if (this.weakness > 0) {
      this.fullness++;
    } else {
      this.resetWeakness();
      if (this.boobs == 0) {
        this.boobs++;
      } else if (this.futa == 0) {
        this.futa++;
      }
    }
  }
  /**pit*/
  public eight() {
    if (this.weakness > 0 || this.corruption > 0 || this.fullness > 0) {
      this.fullness += 2;
      this.boobs++;
    }
    this.resetWeakness();
  }

  private resetWeakness() {
    this.weakness = 0;
  }

  public execute(operations: string) {
    for (const op of operations) {
      switch (op) {
        case "1":
          this.one();
          break;
        case "2":
          this.two();
          break;
        case "3":
          this.three();
          break;
        case "4":
          this.four();
          break;
        case "5":
          this.five();
          break;
        case "6":
          this.six();
          break;
        case "7":
          this.seven();
          break;
        case "8":
          this.eight();
          break;
        default:
          throw new Error(`Unknown operation: ${op}`);
          break;
      }
    }
  }
}
