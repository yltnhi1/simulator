/*
    Copyright 2024 Gironta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

export function clamp(value: number, min: number, max: number) {
  return Math.min(Math.max(value, min), max);
}

export function baseNStrings(base: number, length: number) {
  if (base > 36) throw new Error("I'm afraid I can't do that");
  if (base < 1 || length == 0) return [];
  if (base == 1) return ["".padStart(length, "0")];
  return Array(base ** length)
    .fill(0)
    .map((_, i) => i)
    .map((n) => n.toString(base).padStart(length, "0"));
}
