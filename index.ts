/*
    Copyright 2024 Gironta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

import * as fs from "fs";

import { permute } from "./src/permute";
import { baseNStrings } from "./src/util";
import Simulator from "./src/simulator";

function solve() {
  const simulator = new Simulator();
  const endStates = baseNStrings(3, 4);
  const solutions = new Map<string, string[]>(
    endStates.map((state) => [state, []])
  );
  const permutations = permute([1, 2, 3, 4, 5, 6, 7, 8]);
  for (const permutation of permutations) {
    const command = permutation.join("");
    simulator.reset();
    simulator.execute(command);
    if (solutions.has(simulator.state)) {
      solutions.get(simulator.state)!.push(command);
    } else {
      solutions.set(simulator.state, [command]);
    }
  }
  const output = fs.createWriteStream("solutions.txt");
  for (const [key, value] of solutions) {
    output.write(`${key}: ${value.join(",")}\n`);
  }
  output.close();
  console.log("Done");
}

function execute(command: string) {
  const simulator = new Simulator();
  for (const character of command) {
    simulator.execute(character);
    console.log(`Execute ${character}. New state is ${simulator.state}`);
  }
  console.log("Done");
}

// take 1 command line parameter, a string of numbers representing the simulation
function main(argv: string[]) {
  if (argv.length < 1) {
    console.log("Usage: node index.js <string of numbers>");
    return;
  }

  if (argv[0] === "solve") {
    solve();
  } else {
    execute(argv[0]);
  }
}

main(process.argv.slice(2));
